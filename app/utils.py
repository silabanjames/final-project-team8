import datetime
import uuid

from sqlalchemy import create_engine, text, Table, MetaData

psql_creds = {
    "host": "34.87.64.239",
    "port": 5432,
    "user": "users",
    "pass": "password",
    "db": "team-8-db",
}

def get_engine():
    """Creating SQLite Engine to interact"""

    # UNCOMMENT THIS BEFORE DEPLOY
    engine_uri = "postgresql+psycopg2://{}:{}@{}:{}/{}".format(
        psql_creds["user"],
        psql_creds["pass"],
        psql_creds["host"],
        psql_creds["port"],
        psql_creds["db"],
    )

    return create_engine(engine_uri, future=True)


def run_query(query, commit: bool = False):
    """Runs a query against the given SQLite database.

    Args:
        commit: if True, commit any data-modification query (INSERT, UPDATE, DELETE)
    """
    engine = get_engine()
    if isinstance(query, str):
        query = text(query)

    with engine.connect() as conn:
        if commit:
            conn.execute(query)
            conn.commit()
        else:
            return [dict(row) for row in conn.execute(query)]

def created_at():
    x = datetime.datetime.now()
    return x.strftime("%a, %d %B %Y")


def gen_id():
    return str(uuid.uuid4())

def get_ship_price(shipping_method, total_price):
    if shipping_method == "regular" or shipping_method == "same day":
        if total_price < 200000:
            shipping_price = 0.15 * total_price
        else:
            shipping_price = 0.2 * total_price

    elif shipping_method == "next day":
        if total_price < 300000:
            shipping_price = 0.2 * total_price
        else:
            shipping_price = 0.25 * total_price

    return int(shipping_price)
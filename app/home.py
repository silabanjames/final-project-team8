from flask import Blueprint
from sqlalchemy import MetaData, Table, select, desc, and_
from utils import run_query, get_engine


home_bp = Blueprint("home", __name__, url_prefix="/home")

@home_bp.route("/banner", methods=["GET"])
def get_banner():
    products = Table("products", MetaData(bind=get_engine()), autoload=True)
    product_images = Table("product_images", MetaData(bind=get_engine()), autoload=True)
    product = run_query(select(products).where(products.c.deleted == False).limit(3).order_by(desc(products.c.sold)))
    data = []
    for i in range(len(product)):
        image = run_query(select(product_images).where(product_images.c.product_id == product[i]["id_product"]))
        if len(image) == 0:
            img = "/image/no-image.png"
        else:
            img = image[0]["image"]
        dict = {
            "id": product[i]["id_product"],
            "image": img,
            "title": product[i]["product_name"]
        }
        data.append(dict)
    return {"data": data}, 200


@home_bp.route("/category", methods=["GET"])
def get_category():
    categories = Table("categories", MetaData(bind=get_engine()), autoload=True)
    products = Table("products", MetaData(bind=get_engine()), autoload=True)
    product_images = Table("product_images", MetaData(bind=get_engine()), autoload=True)

    category = run_query(select(categories).where(categories.c.deleted == False))
    data = []
    for i in range(len(category)):
        product = run_query(select(products).where(and_(products.c.category_id == category[i]["id_category"], products.c.deleted == False)))
        if len(product) == 0:
            img = "/image/no-image.png"
        else:
            image = run_query(select(product_images).where(product_images.c.product_id == product[0]["id_product"]))
            if len(image) == 0:
                img = "/image/no-image.png"
            else:
                img = image[0]["image"]
        dict = {
            "id": category[i]["id_category"],
            "image": img,
            "title": category[i]["category_name"]
        }
        data.append(dict)
    return {"data": data}, 200

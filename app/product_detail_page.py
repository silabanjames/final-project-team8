from flask import Blueprint, request
from sqlalchemy import MetaData, Table, select, update, and_, insert
from utils import run_query, gen_id, get_engine
from product_list import products_bp


@products_bp.route("/<product_id>", methods=["GET"])
def get_product_details(product_id):
    categories = Table("categories", MetaData(bind=get_engine()), autoload=True)
    products = Table("products", MetaData(bind=get_engine()), autoload=True)
    product_images = Table("product_images", MetaData(bind=get_engine()), autoload=True)

    product = run_query(select(products).where(and_(products.c.id_product == product_id, products.c.deleted == False)))
    category = run_query(select(categories).where(categories.c.id_category == product[0]["category_id"]))

    image = run_query(select(product_images).where(product_images.c.product_id == product_id))
    image_list = []
    for i in range(len(image)):
        image_list.append(image[i]["image"])

    size = str(product[0]["size"]).split(", ")

    return {
        "data": {
            "id": product[0]["id_product"],
            "title": product[0]["product_name"],
            "size": size,
            "product_detail": product[0]["description"],
            "price": product[0]["price"],
            "images_url": image_list,
            "category_id": product[0]["category_id"],
            "category_name": category[0]["category_name"],
        }
    }, 200

cart_bp = Blueprint("cart", __name__, url_prefix="/cart")

@cart_bp.route("", methods=["POST"])
def add_to_cart():
    body = request.json
    headers = request.headers
    id = body.get("id")
    quantity = int(body.get("quantity"))
    size = body.get("size")
    token = headers.get("Authentication")

    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    carts = Table("carts", MetaData(bind=get_engine()), autoload=True)

    user = run_query(select(users).where(and_(users.c.token == token, users.c.type == "buyer")))
    cart = run_query(select(carts).where(and_(carts.c.size == size, carts.c.product_id == id, carts.c.user_id == user[0]["id_user"])))

    if len(user) == 0:
        return {"message": "error, user is invalid"}, 400
    elif len(cart) == 0:
        data = {"id_cart": gen_id(), "user_id": user[0]["id_user"], "product_id": id,  "quantity": quantity, "size": size, "user_id": user[0]["id_user"], "deleted": False, "order_id": gen_id()}
        run_query(insert(carts).values(data), commit=True)
    else:
        run_query(update(carts).values({"quantity": cart[0]["quantity"]+quantity}).where(and_(carts.c.user_id == user[0]["id_user"], carts.c.size == size, carts.c.product_id == id)), commit=True)
        
    return {"message": "success, Item added to cart"}, 201


import base64
from io import BytesIO
import random
import string

from flask import Blueprint, request, json
from sqlalchemy import Table, MetaData, and_, delete, insert, asc, desc, select, update
from utils import get_engine, run_query, gen_id
from PIL import Image
from product_list import products_bp, categories_bp

orders_bp = Blueprint("orders", __name__, url_prefix="/orders")


@orders_bp.route("", methods=["GET"])
def get_orders():
    args = request.args
    headers = request.headers
    sort_by = args.get("sort_by")
    page = args.get("page")
    page_size = args.get("page_size")
    is_admin = args.get("is_admin", type=bool)
    token = headers.get("Authentication")

    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    orders = Table("orders", MetaData(bind=get_engine()), autoload=True)
    user = run_query(select(users).where(and_(users.c.token == token, users.c.type == "seller")))

    if len(user) == 0 and is_admin == False:
        return {"message": "error, user is not admin"}, 400
    
    else:
        if sort_by == None or page == None or page_size == None:
            order = run_query(select(orders))
        else:
            if sort_by == "price a_z":
                sort_by = asc(orders.c.total_price)
            else:
                sort_by = desc(orders.c.total_price)

            order = run_query(select(orders).limit(int(page)*int(page_size)).order_by(sort_by))

    data = []
    for i in range(len(order)):
        buyer = run_query(select(users).where(users.c.id_user == order[i]["user_id"]))
        dict = {}
        dict["id"] = order[i]["id_order"]
        dict["user_name"] = buyer[0]["name"]
        dict["created_at"] = order[i]["created_at"]
        dict["user_id"] = order[i]["user_id"]
        dict["user_email"] = buyer[0]["email"]
        dict["total"] = order[i]["total_product_price"]
    
        data.append(dict)

    return {"data": data}, 200


@products_bp.route("", methods=["POST"])
def create_product():
    body = json.loads(request.data)
    headers = request.headers
    product_name = body.get("product_name")
    description = body.get("description")
    images = body.get("images")
    condition = body.get("condition")
    category = body.get("category")
    price = body.get("price")
    token = headers.get("Authentication")

    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    products = Table("products", MetaData(bind=get_engine()), autoload=True)
    product_images = Table("product_images", MetaData(bind=get_engine()), autoload=True)

    user = run_query(select(users).where(and_(users.c.token == token, users.c.type == "seller")))
    product = run_query(select(products).where(and_(products.c.product_name == product_name, products.c.category_id == category, products.c.condition == condition)))

    if len(user) == 0:
        return {"message": "error, user is not admin"}, 400
    elif len(product) != 0:
        return {"message": "error, Product is already exists"}, 409
    else:
        data = {
            'id_product': gen_id(),
            'category_id': category,
            'product_name': product_name,
            'description': description,
            'condition': condition,
            'price': price,
            'size': "S, M, L, XL",
            'sold': 0,
            'deleted': False
        }
        run_query(insert(products).values(data), commit=True)

        if images :
            for image in images:
                base64_bytes = image.split(',')
                imag = bytes(base64_bytes[1], encoding="ascii")
                file_name = ''.join(random.choices(string.ascii_lowercase + string.digits, k=20)) + f".{base64_bytes[0].split('/')[1].split(';')[0]}"

                img = Image.open(BytesIO(base64.b64decode(imag)))
                img.save('images/'+f'{file_name}')

                run_query(insert(product_images).values({'id_product_images': gen_id(), "product_id": data["id_product"], "image": f"/image/{file_name}"}), commit=True)
                
        return {"message": "Product added"}, 201
    


@products_bp.route("", methods=["PUT"])
def update_product():
    header = request.headers
    token = header.get("Authentication")
    body = json.loads(request.data)
    product_name = body.get('product_name')
    description = body.get('description')
    images = body.get('images')
    condition = body.get('condition')
    category_id = body.get('category')
    price = body.get('price')
    product_id = body.get("product_id")
    
    products = Table("products", MetaData(bind=get_engine()), autoload=True)
    product_images = Table("product_images", MetaData(bind=get_engine()), autoload=True)


    product = run_query(select(products).where(and_(products.c.category_id == category_id, products.c.product_name == product_name, products.c.condition == condition, products.c.deleted == False)))
    if len(product) != 0 :
        return {"message": "error, Product is already exists"}, 409
    else:
        data = {
            'id_product': product_id,
            'category_id': category_id,
            'product_name': product_name,
            'description': description,
            'condition': condition,
            'price': price
        }
        run_query(update(products).values(data).where(products.c.id_product == product_id), commit=True)
        
        if images :
            for image in images:
                try:
                    base64_bytes = image.split(',')
                    imag = bytes(base64_bytes[1], encoding="ascii")
                    file_name = ''.join(random.choices(string.ascii_lowercase + string.digits, k=20)) + f".{base64_bytes[0].split('/')[1].split(';')[0]}"

                    img = Image.open(BytesIO(base64.b64decode(imag)))
                    img.save('images/'+f'{file_name}')
                    
                    run_query(delete(product_images).where(product_images.c.product_id == product_id), commit=True)
                    run_query(insert(product_images).values({'id_product_images': gen_id(), "product_id": product_id, "image": f"/image/{file_name}"}), commit=True)
                except:
                    images == None
        return {"message": "Product updated"}, 200


@products_bp.route("/<product_id>", methods=["DELETE"])
def delete_product(product_id):
    products = Table("products", MetaData(bind=get_engine()), autoload=True)
    run_query(update(products).values({"deleted": True}).where(products.c.id_product == product_id), commit=True)
    return {"message": "Product deleted"}, 200


@categories_bp.route("", methods=["POST"])
def create_category():
    body = json.loads(request.data)
    category_name = body.get("category_name")
    data = {
        "id_category" : gen_id(),
        "category_name" : category_name,
        "deleted" : False
    }
    categories = Table("categories", MetaData(bind=get_engine()), autoload=True)
    run_query(insert(categories).values(data), commit=True)
    return {"message": "Category added"}, 201
    


@categories_bp.route("/<category_id>", methods=["PUT"])
def update_category(category_id):
    body = json.loads(request.data)
    category_name = body.get("category_name")

    categories = Table("categories", MetaData(bind=get_engine()), autoload=True)
    run_query(update(categories).values({"category_name": category_name}).where(categories.c.id_category == category_id), commit=True)
    return {"message": "Category updated"}, 200


@categories_bp.route("/<category_id>", methods=["DELETE"])
def delete_category(category_id):
    categories = Table("categories", MetaData(bind=get_engine()), autoload=True)
    run_query(update(categories).values({"deleted": True}).where(categories.c.id_category == category_id), commit=True)
    return {"message": "Category deleted"}, 200


sales_bp = Blueprint("sales", __name__, url_prefix="/sales")

@sales_bp.route("", methods=["GET"])
def get_total_sales():
    headers = request.headers
    token = headers.get("Authentication")
    
    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    user = run_query(select(users).where(and_(users.c.token == token, users.c.type == "seller")))

    return {
        "data": {
            "total": user[0]["balance"]
        }
    }, 200
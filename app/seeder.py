from sqlalchemy import delete, insert, select, Table, MetaData
from passlib.hash import sha256_crypt
from utils import run_query, created_at, gen_id, get_ship_price, get_engine


def create_seeder():
    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    shipping_addresses = Table("shipping_addresses", MetaData(bind=get_engine()), autoload=True)
    categories = Table("categories", MetaData(bind=get_engine()), autoload=True)
    products = Table("products", MetaData(bind=get_engine()), autoload=True)
    product_images = Table("product_images", MetaData(bind=get_engine()), autoload=True)
    carts = Table("carts", MetaData(bind=get_engine()), autoload=True)
    orders = Table("orders", MetaData(bind=get_engine()), autoload=True)
    
    # Delete seed
    run_query(delete(users), commit=True)
    run_query(delete(categories), commit=True)
    run_query(delete(products), commit=True)
    run_query(delete(product_images), commit=True)

    # Table Seller
    user_data = [
        {
            "id_user": f"{gen_id()}",
            "name": "Admin Team 8",
            "email": "admin@gmail.com",
            "phone_number": "088888888888", 
            "password": f"{sha256_crypt.hash('Password12345')}",
            "type" : "seller",
            "balance" : 100000
        },
        {
            "id_user": f"{gen_id()}",
            "name": "User Team 8",
            "email": "user@gmail.com",
            "phone_number": "088888888888", 
            "password": f"{sha256_crypt.hash('Password12345')}",
            "type" : "buyer",
            "balance" : 100000
        }
    ]
    run_query(insert(users).values(user_data), commit=True)


    # Table Categories
    categories_data = [
        {   
            "id_category": f"{gen_id()}",
            "category_name": "Boots",
            "deleted": False,
        },
        {   
            "id_category": f"{gen_id()}",
            "category_name": "Sandal",
            "deleted": False,
        },
        {   
            "id_category": f"{gen_id()}",
            "category_name": "Shoes",
            "deleted": False,
        },
        {   
            "id_category": f"{gen_id()}",
            "category_name": "Snikers",
            "deleted": False,
        }
    ]
    run_query(insert(categories).values(categories_data), commit=True)

    # Tabel Product
    category = run_query(select(categories))
    for i in range(len(category)):
        products_data = [
            {
                "id_product": f"{gen_id()}",
                "category_id": f"{category[i]['id_category']}",
                "product_name": f"Product {i+1} {category[i]['category_name']}",
                "description": f"This is description of Product {i+1}",
                "condition": "new",
                "price": 50000,
                "size": "S, M, L",
                "sold": i+1,
                "deleted": False,
            },
            {
                "id_product": f"{gen_id()}",
                "category_id": f"{category[i]['id_category']}",
                "product_name": f"Product {i+1} {category[i]['category_name']}",
                "description": f"This is description of Product {i+1}",
                "condition": "used",
                "price": 185000,
                "size": "S, M, L",
                "sold": i+1,
                "deleted": False,
            },
        ]
        run_query(insert(products).values(products_data), commit=True)

    # Tabel Product Images
    product = run_query(select(products))
    for i in range(len(product)):
        product_images_data = {
            "id_product_images": f"{gen_id()}",
            "product_id": f"{product[i]['id_product']}",
            "image": f"/image/img_product{i+1}.jpg"
        }
        run_query(insert(product_images).values(product_images_data), commit=True)

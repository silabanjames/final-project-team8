from flask import Blueprint, request, json
from sqlalchemy import and_, or_, asc, desc, select, Table, MetaData
from utils import get_engine, run_query
from PIL import Image
# from ai.main import Main

products_bp = Blueprint("products", __name__, url_prefix="/products")


@products_bp.route("", methods=["GET"])
def get_product():
    args = request.args
    sort_by = args.get("sort_by")
    category = args.get("category")
    product_name = args.get("product_name")
    condition = args.get("condition")
    page = args.get("page")
    page_size = args.get("page_size")
    price = args.get("price")

    products = Table("products", MetaData(bind=get_engine()), autoload=True)
    product_images = Table("product_images", MetaData(bind=get_engine()), autoload=True)
    
    if len(args) == 0 :
        product_list = run_query(select(products).where(products.c.deleted == False))
    
    else :
        if category == None and product_name == "undefined" and condition == None and price == None:
            product_list = []
        elif product_name != "undefined" or condition != None:
                if product_name != "undefined":
                    product_name = (products.c.title == product_name)
                else:
                    product_name = (products.c.deleted == False)

                if condition != None:
                    condition = condition.split(",")
                    if len(condition) == 1:
                        condition = (products.c.condition == condition[0])
                    else:
                        condition = (or_(products.c.condition == condition[0], products.c.condition == condition[1]))
                else:
                    condition = (products.c.deleted == False)
                
                if category != None:
                    product_list = []
                    category = category.split(",")
                    for i in range(len(category)):
                        data = run_query(select(products).where(and_(condition, product_name, products.c.category_id == category[i], products.c.deleted == False)))
                        for x in data:
                            product_list.append(x)
                else:
                    product_list = run_query(select(products).where(and_(condition, product_name, products.c.deleted == False)))
                
                if sort_by == "Price a_z":
                    product_list = sorted(product_list, key=lambda d: d['price']) 
                else:
                    product_list = sorted(product_list, key=lambda d: d['price'], reverse=True)
                product_list =  product_list[0:(int(page)*int(page_size))]
        else:
            if price != None:
                price = price.split(",")
                if category != None:
                    category = category.split(",")
                    product_list = []
                    for i in range(len(category)):
                        data = run_query(select(products).where(and_(products.c.category_id == category[i], products.c.price >= price[0], products.c.price <= price[1], products.c.deleted == False)))
                        for x in data:
                            product_list.append(x)
                else:
                    product_list = run_query(select(products).where(and_(products.c.price >= price[0], products.c.price <= price[1], products.c.deleted == False)))
                
                if sort_by == "Price a_z":
                    product_list = sorted(product_list, key=lambda d: d['price']) 
                else:
                    product_list = sorted(product_list, key=lambda d: d['price'], reverse=True)
                product_list = product_list[0:(int(page)*int(page_size))]
            else:
                if category != None:
                    category = category.split(",")
                    product_list = []
                    for i in range(len(category)):
                        data = run_query(select(products).where(and_(products.c.category_id == category[i], products.c.deleted == False)))
                        for x in data:
                            product_list.append(x)
                else:
                    product_list = run_query(select(products).where(and_(products.c.deleted == False)))  

                if sort_by == "Price a_z":
                    product_list = sorted(product_list, key=lambda d: d['price']) 
                else:
                    product_list = sorted(product_list, key=lambda d: d['price'], reverse=True)
                product_list =  product_list[0:(int(page)*int(page_size))]

    data = []
    for i in range(len(product_list)):
        dict = {}

        image_list = run_query(select(product_images).where(
            product_images.c.product_id == product_list[i]["id_product"]))

        if len(image_list) == 0 :
            dict["image"] = "/image/no-image.png"
        else:
            dict["image"] = image_list[0]["image"]

        dict["id"] = product_list[i]["id_product"]
        dict["title"] = product_list[i]["product_name"]
        dict["price"] = product_list[i]["price"]

        data.append(dict)

    return {"data": data, "total_rows": len(product_list)}, 200


@products_bp.route("/search_image", methods=["POST"])
def search_image():
    body = request.json
    image = body.get('image')

    # result = Main(base64_image=image)
    result = image

    categories = Table("categories", MetaData(bind=get_engine()), autoload=True)
    category = run_query(select(categories).where(categories.c.category_name == result))

    return {"category": category[0]["id_category"]}, 200



categories_bp = Blueprint("categories", __name__, url_prefix="/categories")

@categories_bp.route("", methods=["GET"])
def get_category():
    # IMPLEMENT THIS
    categories = Table("categories", MetaData(bind=get_engine()), autoload=True)
    category = run_query(select(categories).where(categories.c.deleted == False))

    data = []
    for i in range(len(category)):
        data_temp = {}
        data_temp["id"] = category[i]["id_category"]
        data_temp["title"] = category[i]["category_name"]
        data.append(data_temp)
    
    return {"data": data}, 200
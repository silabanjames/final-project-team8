from flask import Blueprint, request
from sqlalchemy import Table, MetaData, and_, delete, insert, select, update
from product_detail_page import cart_bp
from utils import run_query, created_at, gen_id, get_engine, get_ship_price


@cart_bp.route("", methods=["GET"])
def get_user_cart():
    headers = request.headers
    token = headers.get("Authentication")

    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    carts = Table("carts", MetaData(bind=get_engine()), autoload=True)

    user = run_query(select(users).where(and_(users.c.token == token, users.c.type == "buyer")))
    cart = run_query(select(carts).where(and_(carts.c.user_id == user[0]["id_user"], carts.c.deleted == False)))

    data = []
    for i in range(len(cart)):
        dict = {}
        dict["id"] = cart[i]["id_cart"]
        dict["details"] = {
            "quantity": cart[i]["quantity"],
            "size": cart[i]["size"]
        }

        products = Table("products", MetaData(bind=get_engine()), autoload=True)
        product_images = Table("product_images", MetaData(bind=get_engine()), autoload=True)
        
        product = run_query(select(products).where(products.c.id_product == cart[i]["product_id"]))
        dict["price"] = product[0]["price"]
        dict["name"] = product[0]["product_name"]

        product_image = run_query(select(product_images).where(product_images.c.product_id == cart[i]["product_id"]))
        if len(product_image) == 0 :
            continue
        elif len(product_image) == 1:
            dict["image"] = product_image[0]["image"]
        else :
            dict["image"] = []
            for j in range(len(product_image)):
                dict["image"].append(product_image[j]["image"])

        data.append(dict)

    return {
        "data": data,
        "total_rows": len(data)
    }, 200

@cart_bp.route("/<cart_id>", methods=["DELETE"])
def delete_cart_item(cart_id):
    carts = Table("carts", MetaData(bind=get_engine()), autoload=True)
    run_query(delete(carts).where(carts.c.id_cart == cart_id), commit=True)
    return {"message": "Cart deleted"}, 200

user_bp = Blueprint("user", __name__, url_prefix="/user")

@user_bp.route("/shipping_address", methods=["GET"])
def get_user_sa():
    header = request.headers
    token = header.get("Authentication")

    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    shipping_addresses = Table("shipping_addresses", MetaData(bind=get_engine()), autoload=True)
    user = run_query(select(users).where(users.c.token == token))
    shipping_address = run_query(select(shipping_addresses).where(shipping_addresses.c.user_id == user[0]["id_user"]))

    return {
        "data":
            {
                "id": shipping_address[0]["id_address"],
                "name": shipping_address[0]["name"],
                "phone_number": shipping_address[0]["phone_number"],
                "address": shipping_address[0]["address"],
                "city": shipping_address[0]["city"]
            }
    }, 200


shipping_price_bp = Blueprint("shipping_price", __name__, url_prefix="/shipping_price")

@shipping_price_bp.route("", methods=["GET"])
def get_shipping_price():
    header = request.headers
    token = header.get("Authentication")

    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    carts = Table("carts", MetaData(bind=get_engine()), autoload=True)
    products = Table("products", MetaData(bind=get_engine()), autoload=True)

    user = run_query(select(users).where(users.c.token == token))

    carts_user = run_query(select(carts, products).join(products, carts.c.product_id == products.c.id_product).where(carts.c.user_id == user[0]['id_user']))
    total_price = 0
    for row in carts_user:
        total_price+=int(row['price'])*int(row['quantity'])
    
    regular_price = get_ship_price(shipping_method="regular", total_price=total_price)
    next_day_price = get_ship_price(shipping_method="next day", total_price=total_price)


    return {
        "data" : [
            {
                "name":"regular",
                "price":regular_price
            },
            {
                "name":"next day",
                "price":next_day_price
            }
        ]
    }, 200


order_bp = Blueprint("order", __name__, url_prefix="/order")

@order_bp.route("", methods=["POST"])
def create_order():
    headers = request.headers
    token = headers.get("Authentication")
    body = request.json
    shipping_method = body.get("shipping_method")

    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    carts = Table("carts", MetaData(bind=get_engine()), autoload=True)
    products = Table("products", MetaData(bind=get_engine()), autoload=True)
    orders = Table("orders", MetaData(bind=get_engine()), autoload=True)
    
    user = run_query(select(users).where(and_(users.c.token == token, users.c.type == "buyer")))
    cart = run_query(select(carts).where(and_(carts.c.user_id == user[0]["id_user"], carts.c.deleted == False))) 
    carts_user = run_query(select(carts, products).join(products, carts.c.product_id == products.c.id_product).where(carts.c.user_id == user[0]['id_user']))
    
    total_product_price = 0
    for row in carts_user:
        total_product_price+=int(row['price'])*int(row['quantity'])

    shipping_price = get_ship_price(shipping_method, total_product_price)
    total_price = total_product_price + shipping_price
    diff = total_price - user[0]["balance"]

    if diff > 0:
        return {"message": f"error, Please top up {diff}"}, 400
    else:
        data = {
            'id_order': gen_id(),
            'user_id': user[0]["id_user"],
            'shipping_method': shipping_method,
            'shipping_price': shipping_price,
            'total_product_price': total_product_price,
            'created_at': created_at(),
            'status': "waiting"
        }
        run_query(insert(orders).values(data), commit=True)

        for i in range(len(cart)):
            product = run_query(select(products).where(products.c.id_product == cart[i]["product_id"]))
            run_query(update(products).values({"sold" : product[0]["sold"] + cart[i]["quantity"] }).where(products.c.id_product == cart[i]["product_id"]), commit=True)
            run_query(update(carts).values({"deleted" : True, "order_id": data["id_order"]}).where(carts.c.id_cart == cart[i]["id_cart"]), commit=True)

        run_query(update(users).values({"balance" : abs(diff)}).where(and_(users.c.token == token, users.c.type == "buyer")), commit=True)
        seller = run_query(select(users).where(users.c.type == "seller"))
        run_query(update(users).values({"balance" : seller[0]["balance"] + total_product_price}).where(and_(users.c.id_user == seller[0]["id_user"], users.c.type == "seller")), commit=True)

        return {"message": "Order success"}, 201
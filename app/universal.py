from flask import Blueprint, send_file

# this means that you can group all endpoints with prefix "/home" together
img_bp = Blueprint("image", __name__, url_prefix="/image")

# Universal Endpoints Image
@img_bp.route("/<image_name>", methods=["GET"])
def get_image(image_name):
    extension = image_name.split(".")
    extension = extension[len(extension)-1]
    return send_file(f"images/{image_name}", mimetype=f'image/{extension}')